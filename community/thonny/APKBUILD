# Contributor: Justin Berthault <justin.berthault@zaclys.net>
# Maintainer: Justin Berthault <justin.berthault@zaclys.net>
pkgname=thonny
pkgver=4.1.2
pkgrel=0
pkgdesc="Python IDE for beginners"
url="https://thonny.org"
arch="noarch !s390x" # py3-pylint
license="MIT"
depends="
	openssl
	py3-astroid
	py3-asttokens
	py3-docutils
	py3-filelock
	py3-gpep517
	py3-jedi
	py3-mypy
	py3-pylint
	py3-pyperclip
	py3-pyserial
	py3-send2trash
	py3-setuptools
	py3-wheel
	python3-tkinter
	"
subpackages="$pkgname-lang $pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/t/thonny/thonny-$pkgver.tar.gz"
options="!check" # Tests need a DISPLAY

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 setup.py test
}

package() {
	local pythonpath="$(python3 -c \
		"from sysconfig import get_path; print(get_path('platlib'))")"

	python3 -m installer -d "$pkgdir" .dist/*.whl
	rm -r "$pkgdir$pythonpath"/thonny/vendored_libs/filelock

	local appid="org.thonny.Thonny"
	install -Dm644 packaging/linux/$appid.desktop \
		-t "$pkgdir"/usr/share/applications
	install -Dm644 packaging/linux/$appid.appdata.xml \
		-t "$pkgdir"/usr/share/metainfo

	local size
	for size in 16 22 32 48 64 128 192 256; do
		install -Dm644 packaging/icons/thonny-"$size"x"$size".png \
			"$pkgdir"/usr/share/icons/hicolor/"$size"x"$size"/apps/thonny.png
	done
}

lang() {
	local pythonpath="$(python3 -c \
		"from sysconfig import get_path; print(get_path('platlib'))")"

	langdir="$pythonpath"/thonny/locale
	default_lang
}

sha512sums="
570a102d2b2644cf4d4a149475471867ed683a3fed719c1e03a99eaf42078def89297da140db3a6a677ebbef2f6dc55059b0a14e929a969051c7d819e40f0074  thonny-4.1.2.tar.gz
"
