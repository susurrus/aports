# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=libkgapi
pkgver=23.08.0
pkgrel=0
pkgdesc="LibKGAPI is a KDE-based library for accessing various Google services via their public API"
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://community.kde.org/KDE_PIM"
license="LGPL-2.1-only OR LGPL-3.0-only"
depends_dev="
	cyrus-sasl-dev
	kcalendarcore-dev
	kcontacts-dev
	kio-dev
	kwallet-dev
	kwindowsystem-dev
	qt5-qtbase-dev
	qt5-qtwebengine-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/pim/libkgapi.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkgapi-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	# calendar-freebusyqueryjobtest, contacts-contactfetchjobtest,
	# contacts-contactcreatejobtest and contacts-contactmodifyjobtest are broken
	# tasks-taskfetchjobtest fails on the builders
	# contacts-contactfetchjobtest: https://gitlab.alpinelinux.org/alpine/aports/-/issues/11787
	# calendar-event-*: UTC+02 != UTC+02:00
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "(tasks-taskfetchjob|calendar-freebusyqueryjob|contacts-contact(fetchjob|createjob|modifyjob)|calendar-event(createjob|modifyjob|fetchjob))test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
d2e039d967431f65b30e791dded8f23f494d250a17cb9383ca6b0caf95d7f49a36cc1ce0a08f8fdbc922b7f0c0af9b6bbc06a52f212d8e8bd4e5788c9042a4ef  libkgapi-23.08.0.tar.xz
"
