# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=libkipi
pkgver=23.08.0
pkgrel=0
pkgdesc="KDE Image Plugin Interface library"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.digikam.org/"
license="GPL-2.0-or-later AND LGPL-2.0-or-later"
depends_dev="
	kconfig-dev
	kservice-dev
	kxmlgui-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	libjpeg-turbo-dev
	libkexiv2-dev
	samurai
	tiff-dev
	"
_repo_url="https://invent.kde.org/graphics/libkipi.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkipi-$pkgver.tar.xz"
subpackages="$pkgname-dev"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
b62a975a6387b8ae923a9bcf6d04bf13c327ddd618d72001ff705d232a15b3654ccd5db48209add9b6bc8436e0b02a5f67a84f5dc0c1c481ea167b0761ca4787  libkipi-23.08.0.tar.xz
"
