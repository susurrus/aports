# Contributor: Duncan Bellamy <dunk@denkimushi.com>
# Maintainer: Duncan Bellamy <dunk@denkimushi.com>
pkgname=py3-softlayer
pkgver=6.1.8
pkgrel=0
pkgdesc="library for SoftLayer's API"
url="https://github.com/softlayer/softlayer-python"
arch="noarch"
license="MIT"
depends="py3-click py3-prompt_toolkit py3-prettytable3 py3-pygments py3-requests py3-rich py3-urllib3"
makedepends="py3-gpep517 py3-setuptools py3-wheel"
checkdepends="py3-pytest py3-pytest-mock py3-testtools py3-softlayer-zeep python3-tkinter"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/softlayer/softlayer-python/archive/v$pkgver.tar.gz"
builddir="$srcdir/softlayer-python-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" .dist/*.whl

	rm -rf "$pkgdir"/usr/lib/python3*/site-packages/tests
}

sha512sums="
6c014a7b77db644811c5c564d7e40bffdb56fd7c039cf4da70f7bb2985b41cb79c65e5b726f61566963e083c1b9d039ea2aa2b582225140083c140fdee0febfc  py3-softlayer-6.1.8.tar.gz
"
