# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=spectacle
pkgver=23.08.0
pkgrel=0
pkgdesc="Application for capturing desktop screenshots"
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine -> purpose
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kde.org/applications/utilities/org.kde.spectacle"
license="GPL-2.0-only"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kdeclarative-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	knewstuff-dev
	knotifications-dev
	kpipewire-dev
	kwayland-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	libkipi-dev
	libxcb-dev
	pipewire-dev
	purpose-dev
	qt5-qtbase-dev
	qt5-qtx11extras-dev
	samurai
	xcb-util-cursor-dev
	xcb-util-image-dev
	xcb-util-renderutil-dev
	"
_repo_url="https://invent.kde.org/graphics/spectacle.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/spectacle-$pkgver.tar.xz
	spectacle.desktop
	"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	install -Dm644 "$srcdir"/spectacle.desktop -t "$pkgdir"/etc/xdg/autostart/

	# We don't ship systemd
	rm -r "$pkgdir"/usr/lib/systemd
}
sha512sums="
caa1e2dd7989824d0df25aefb852d7c2a94cd77ee5d04c2cf94914ee42409cf85bd0f4be125fed3ddff08e4b91f44a211102dca12cbd6188253005c1a8c04179  spectacle-23.08.0.tar.xz
7c563d811f30d26f83e01a465e803b95167c5b2b842315257216ab282e07c69e7582a14d7f429cd19678199179ad8f3f2854265092f5a4c9ce9b65c87ed3849d  spectacle.desktop
"
