# Maintainer: Will Sinatra <wpsinatra@gmail.com>
pkgname=pushgateway
pkgver=1.6.1
pkgrel=0
pkgdesc="Prometheus push acceptor for ephemeral and batch jobs"
url="https://github.com/prometheus/pushgateway"
license="Apache-2.0"
arch="all"
install="pushgateway.pre-install"
makedepends="go yarn bash"
source="
	$pkgname-$pkgver.tar.gz::https://github.com/prometheus/pushgateway/archive/v$pkgver.tar.gz
	pushgateway.confd
	pushgateway.initd
"
subpackages="$pkgname-openrc"
options="!check" # Broken by integrations we don't care about

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go mod vendor
	go build \
		-trimpath \
		-mod=vendor \
		-ldflags "-extldflags \"$LDFLAGS\""
}

check() {
	go test ./...
}

package() {
	install -Dm755 pushgateway "$pkgdir"/usr/bin/pushgateway

	install -Dm644 "$srcdir"/pushgateway.confd \
		"$pkgdir"/etc/conf.d/pushgateway
	install -Dm755 "$srcdir"/pushgateway.initd \
		"$pkgdir"/etc/init.d/pushgateway
}

sha512sums="
dd7e3d276cce7c21d3a7504cafc9298fc4093d8096f507a82c08468d8ef3be5f89fc269dfdf7a221f10189c073e5e2992df8dbefe045a773f4ef6d438b5567e8  pushgateway-1.6.1.tar.gz
39b797f6a38b02f84f94cfdbc5c860af060b044eaf98d7ec99909c4abcb9bf6112755bbd21460bfda9825b31715e403fedb6c88158072cd79d985429bcb5cf01  pushgateway.confd
8dec09ad9b420da54f6f9434307c07d905c4bb17c2bc68fc4863e2bc2288ae1c8faf5757485ad4b0ebb89a78e907f6ccaf11f8379e8ab71e5e88f7c741657f6f  pushgateway.initd
"
