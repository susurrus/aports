# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kmime
pkgver=23.08.0
pkgrel=0
pkgdesc="Library for handling mail messages and newsgroup articles"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/KDE_PIM"
license="LGPL-2.0-or-later"
depends_dev="
	kcodecs-dev
	ki18n-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
_repo_url="https://invent.kde.org/pim/kmime.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kmime-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build

	# kmime-headertest and kmime-messagetest are broken
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "kmime-(header|message)test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
169ae4cb2cb80f8cac2a562525379e1bb62894e83c11c10f8cc083fc5c0844a73688ea73747d875f055f0dc58174f4855e66b167534c22031392d1b6dedd54e7  kmime-23.08.0.tar.xz
"
