# Contributor: Patrycja Rosa <alpine@ptrcnull.me>
# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=fcitx5
pkgver=5.0.23
pkgrel=1
pkgdesc="Generic input method framework"
url="https://github.com/fcitx/fcitx5"
# armhf: blocked by extra-cmake-modules, xcb-imdkit-dev
arch="all !armhf"
license="LGPL-2.1-or-later"
makedepends="
	cairo-dev
	dbus-dev
	enchant2-dev
	extra-cmake-modules
	fmt-dev
	gdk-pixbuf-dev
	iso-codes
	iso-codes-dev
	iso-codes-lang
	json-c-dev
	libevent-dev
	libxkbcommon-dev
	libxkbfile-dev
	mesa-dev
	pango-dev
	samurai
	wayland-dev
	wayland-protocols
	xcb-imdkit-dev
	xcb-util-keysyms-dev
	xcb-util-wm-dev
	xkeyboard-config-dev
	"
subpackages="$pkgname-lang $pkgname-dev"
source="$pkgname-$pkgver-2.tar.gz::https://github.com/fcitx/fcitx5/archive/$pkgver/fcitx5-$pkgver.tar.gz
	$pkgname-$pkgver-fmt10.patch::https://github.com/fcitx/fcitx5/commit/7fb3a5500270877d93b61b11b2a17b9b8f6a506b.patch
	"
options="!check" # requires working dbus

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	CXXFLAGS="$CXXFLAGS -DNDEBUG" \
	cmake -G Ninja -B build -Wno-dev \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=None \
		$CMAKE_CROSSOPTS
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
9d6daa465f656e489eea2dbe3a46e9263a5459b7c2b91465076c9ad5abd51f53215d3ae17dff9256fa8c7cdfb9ec0ab052ffdd87cee556584e636feaae6f6875  fcitx5-5.0.23-2.tar.gz
097e01b24b9941bf757939fbb1ef78616f23b3fcea2d4eca7d535dd8bbfff8e5772e8718706c9d4adaddacedd43dbe43fef0897433b3ea0f2de90dfcb3a8a5cd  fcitx5-5.0.23-fmt10.patch
"
