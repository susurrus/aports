# Contributor: Paul Bredbury <brebs@sent.com>
# Maintainer: gay <gay@disroot.org>
pkgname=icewm
pkgver=3.4.1
pkgrel=0
pkgdesc="Window manager designed for speed, usability and consistency"
url="https://github.com/ice-wm/icewm"
arch="all"
options="!check" # No test suite
license="LGPL-2.0-only"
subpackages="$pkgname-doc $pkgname-lang"
makedepends="
	alsa-lib-dev
	cmake
	fribidi-dev
	glib-dev
	imlib2-dev
	libao-dev
	libintl
	librsvg-dev
	libsm-dev
	libsndfile-dev
	libxcomposite-dev
	libxdamage-dev
	libxft-dev
	libxinerama-dev
	libxpm-dev
	libxrandr-dev
	markdown
	perl
	samurai
	"
source="$pkgname-$pkgver-2.tar.lz::https://github.com/ice-wm/icewm/releases/download/$pkgver/icewm-$pkgver.tar.lz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCFGDIR=/etc/icewm \
		-DENABLE_NLS=OFF \
		-DCONFIG_IMLIB2=ON \
		-DCONFIG_LIBRSVG=ON \
		-DENABLE_LTO=ON \
		-DDOCDIR=/usr/share/doc/icewm
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
33fcbb487d7f66b3ef6618b5a5996e201657589e465cf2ee2f3220443330451c871f105f61d876148c17aab7e9e1011ce0aff4775aa3c73085879c8feed24908  icewm-3.4.1-2.tar.lz
"
