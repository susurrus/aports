# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=btrfs-progs
pkgver=6.5
pkgrel=0
pkgdesc="BTRFS filesystem utilities"
url="https://btrfs.wiki.kernel.org"
arch="all"
license="GPL-2.0-or-later"
depends_dev="linux-headers"
makedepends="$depends_dev
	acl-dev
	asciidoc
	attr-dev
	e2fsprogs-dev
	eudev-dev
	lzo-dev
	py3-setuptools
	py3-sphinx
	python3-dev
	util-linux-dev
	xmlto
	zlib-dev
	zstd-dev
	"
checkdepends="
	acl
	coreutils
	libaio-dev
	liburing-dev
	losetup
	xz
	"
subpackages="
	$pkgname-doc
	$pkgname-static
	$pkgname-dev
	$pkgname-bash-completion
	py3-$pkgname:py3
	$pkgname-libs
	$pkgname-extra
	"
source="https://www.kernel.org/pub/linux/kernel/people/kdave/btrfs-progs/btrfs-progs-v$pkgver.tar.xz"
builddir="$srcdir/$pkgname-v$pkgver"
# most pass, one fails to build (fsstress renameat2)
options="!check"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--disable-backtrace
	make
}

check() {
	make test
}

package() {
	make DESTDIR="$pkgdir" install install_python

	mkdir -p "$pkgdir"/usr/share/doc/$pkgname
	install -m644 README.md CHANGES \
		"$pkgdir"/usr/share/doc/$pkgname

	mkdir "$pkgdir"/sbin
	local i; for i in btrfs btrfsck fsck.btrfs mkfs.btrfs; do
		mv "$pkgdir"/usr/bin/$i "$pkgdir"/sbin/
	done
	mv "$pkgdir"/usr/bin "$pkgdir"/usr/sbin

	install -Dm644 btrfs-completion \
		"$pkgdir"/usr/share/bash-completion/completions/btrfs
}

py3() {
	pkgdesc="Python 3 bindings for $pkgname"
	provides="py-btrfs-progs=$pkgver-r$pkgrel"  # for backward compatibility
	replaces="py-btrfs-progs"  # for backward compatibility

	amove usr/lib/python*
}

extra() {
	depends="$pkgname"
	pkgdesc="BTRFS filesystem extra utilities"

	amove usr/sbin
}

sha512sums="
7e1578b1f82549f77b55341c20a973da52018b1539da46f92e4a9d9804906ce5e00312c8a02a6047373e4435e661b2e5fc18f5dfbf2d0563cdf62c40de6a4436  btrfs-progs-v6.5.tar.xz
"
