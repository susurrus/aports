# Contributor: Bart Ribbers <bribbers@disroot.org>
# Contributor: Simon Zeni <simon@bl4ckb0ne.ca>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=vulkan-loader
_pkgname=Vulkan-Loader
pkgver=1.3.261.0
pkgrel=0
arch="all"
url="https://www.khronos.org/vulkan"
pkgdesc="Vulkan Installable Client Driver (ICD) Loader"
license="Apache-2.0"
depends_dev="vulkan-headers"
makedepends="$depends_dev
	cmake
	libx11-dev
	libxrandr-dev
	python3
	samurai
	wayland-dev
	"
source="https://github.com/khronosgroup/vulkan-loader/archive/refs/tags/sdk-$pkgver/vulkan-loader-sdk-$pkgver.tar.gz"
subpackages="$pkgname-dbg $pkgname-dev"
builddir="$srcdir/$_pkgname-sdk-$pkgver"
options="!check" # No tests

build() {
	CXXFLAGS="$CXXFLAGS -flto=auto" \
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_INSTALL_SYSCONFDIR=/etc \
		-DCMAKE_INSTALL_DATADIR=share \
		-DCMAKE_SKIP_RPATH=True \
		-DVULKAN_HEADERS_INSTALL_DIR=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
e48108ef03c404b3f2f28f8ec763b1fee99fb8d8f667d1a60f2e16ffc6230fddc6c37c49944425dbfec7cd2a5b5cc8f2faf9ce5e4190a39be85ac9d45a9159b1  vulkan-loader-sdk-1.3.261.0.tar.gz
"
